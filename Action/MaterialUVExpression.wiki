= MaterialUVExpression

Sets a UV transform on the specified material of the target entity.

* **material**: Name of the material to target in the targeted entity.
* **translate_x**: The X translation expression.
* **translate_y**: The Y translation expression.
* **scale_x**: The X scaling expression.
* **scale_y**: The Y scaling expression.
* **rotation**: The rotation expression.
* **expr**: The full expression. When specified the **translate_x/y**, **scale_x/y** and **rotation** properties are ignored.

NOTE: The expressions use the same syntax as the [[Action/EntitySetProperty|EntitySetProperty]] action. As such they can be animated by using the time variable. For example to do a simple sliding animation you could set the **translate_x** property to {{{0.25*time}}}.
