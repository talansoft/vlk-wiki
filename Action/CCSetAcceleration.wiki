= CCSetAcceleration

Sets the acceleration of the target [[Entity/CollideCharacter|CollideCharacter]] entity.
