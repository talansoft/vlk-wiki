= CCSetVelocity

Sets the velocity of the target [[Entity/CollideCharacter|CollideCharacter]] entity.

NOTE: This is probably not what you want, it will instantly change the current velocity of the [[Entity/CollideCharacter|CollideCharacter]] resulting in an visualy abrupt change of speed. To smoothly change the velocity it is better to use the [[Action/CCSetAcceleration|CCSetAcceleration]], [[Action/CCSetDeceleration|CCSetDeceleration]] and [[[[Action/CCSetMaxVelocity|CCSetMaxVelocity]] actions.
