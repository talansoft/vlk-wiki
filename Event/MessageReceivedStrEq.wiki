= MessageReceivedStrEq

Triggered when the entity receives the specified message and the VarA parameter matches the specified string.

Usually used to handle a message sent with the [[Action/EntitySendMessageStr|EntitySendMessageStr]] action.
