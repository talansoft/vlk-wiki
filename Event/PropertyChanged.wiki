= PropertyChanged

Triggered when the specified property of the target entity is modified and matches the specified condition.

Mostly used with the [[Entity/DataTable|DataTable]] entity. But it can also be used with any bound property.
